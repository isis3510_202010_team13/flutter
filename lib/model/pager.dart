import 'package:intl/intl.dart';

class Pager {
    int id;
    int turno;
    DateTime horaPedido;
    String restaurante;

    Pager({
        this.id,
        this.turno,
        this.horaPedido,
        this.restaurante,
    });

    int get idp => id;
    int get turnop => turno;
    DateTime get horaPedidop => horaPedido;
    String get restaurantep => restaurante;
    final formato = DateFormat("yyyy-MM-dd");


    factory Pager.fromJson(Map<String, dynamic> data) => new Pager(
        id: data["id"],
        turno: data["Turno"],
        horaPedido: data["HoraPedido"],
        restaurante: data["Restaurante"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "Turno": turno,
        "HoraPedido": formato.format(horaPedido),
        "Restaurante": restaurante,
    };
}