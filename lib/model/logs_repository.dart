import 'log.dart';
import 'package:intl/intl.dart';

DateFormat df = DateFormat("yyyy-MM-dd HH:mm");

class LogsRepository{
var _logs = <Log>[
  Log(1 ,1, 'restauranteA', df.parse(df.format(DateTime.now())), 'Pager'),
  Log(2, 1, 'restauranteB', df.parse(df.format(DateTime.now())), 'Table'),
  Log(3, 1, 'restauranteC', df.parse(df.format(DateTime.now())), 'Pager'),
  Log(4, 1, 'restauranteD', df.parse(df.format(DateTime.now())), 'Table')
];

List<Log> loadLogs(){
  return _logs;
}
}