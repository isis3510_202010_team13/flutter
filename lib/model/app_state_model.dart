import 'package:flutter/foundation.dart' as foundation;

import 'log.dart';
import 'logs_repository.dart';

class AppStateModel extends foundation.ChangeNotifier{
  List<Log> _logs;
  LogsRepository lr = LogsRepository();

  List<Log> getLogsByUserId(int uid){
    return _logs.where((l) => l.userId == uid);
  }

  void loadLogs(){
    _logs = lr.loadLogs();
    notifyListeners();
  }

  
}
