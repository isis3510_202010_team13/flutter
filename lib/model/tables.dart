import 'package:intl/intl.dart';

class Tables {
    int id;
    int numeroMesa;
    int numeroPersonas;
    DateTime horaReserva;
    String restaurante;

    Tables({
        this.id,
        this.numeroMesa,
        this.numeroPersonas,
        this.horaReserva,
        this.restaurante,
    });

    int get idt => id;
    int get numeroMesat => numeroMesa;
    int get numeroPersonast => numeroPersonas;
    DateTime get horaReservat => horaReserva;
    String get restaurantet => restaurante;
    final formato = DateFormat("yyyy-MM-dd");

    factory Tables.fromJson(Map<String, dynamic> data) => new Tables(
        id: data["id"],
        numeroMesa: data["NumeroMesa"],
        numeroPersonas: data["NumeroPersonas"],
        horaReserva: data["HoraReserva"],
        restaurante: data["Restaurante"],
    );

    Map<String, dynamic> toJson() => {
        
        "id": id,
        "NumeroMesa": numeroMesa,
        "NumeroPersonas": numeroPersonas,
        "HoraReserva": formato.format(horaReserva),
        "Restaurante": restaurante,
    };
}