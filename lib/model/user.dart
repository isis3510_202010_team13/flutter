class User {
    int id;
    String nombre;
    String sexo;
    DateTime nacimiento;
    String email;
    bool restaurante;

    User({
        this.id,
        this.nombre,
        this.sexo,
        this.nacimiento,
        this.email,
    });

    int get idu => id;
    String get nombreu => nombre;
    String get sexou => sexo;
    DateTime get nacimientou => nacimiento;
    String get emailu => email;


    factory User.fromJson(Map<String, dynamic> data) => new User(
        id: data["id"],
        nombre: data["Nombre"],
        sexo: data["Sexo"],
        nacimiento: data["Nacimiento"],
        email: data["email"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "Nombre": nombre,
        "Sexo": sexo,
        "Nacimiento": nacimiento,
        "Email": email,
    };
}