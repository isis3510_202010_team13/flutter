import 'package:flutter/cupertino.dart';

class Log{

int id;
int userId;
String restaurantId;
DateTime date;
String type;

Log(int i, int u, String r, DateTime d, String t){
  this.id = i;
  this.userId = u;
  this.restaurantId = r;
  this.date = d;
  this.type = t;
}

Widget buildName(BuildContext context) => Text(restaurantId);
Widget buildDate(BuildContext context) => Text(date.toString()+" "+ type);
Widget buildType(BuildContext context) => Text(type);

}