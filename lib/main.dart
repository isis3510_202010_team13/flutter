import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'dart:async';
import 'package:path/path.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';
import 'package:funky_munch/model/user.dart';
import 'package:funky_munch/model/pager.dart';
import 'package:funky_munch/model/tables.dart';
import 'package:funky_munch/model/log.dart';
import 'package:funky_munch/model/logs_repository.dart';
import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:connectivity/connectivity.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'qrcode.dart';


var database;

var idm = 13;
var nmesa = 5;
var npersonas = 3;
var hora;
var rest = "El Restaurante";

var pagerActive = false;
var tableActive = false;
var loggedIn = false;
User actual;
final formato = DateFormat("yyyy-MM-dd");
DateTime hpager;
DateTime htable;

var mesa = new Tables(
  id: idm,
  numeroMesa: nmesa,
  numeroPersonas: npersonas,
  horaReserva: DateTime.now(),
  restaurante: rest,
);

LogsRepository lr = LogsRepository();
List<Log> userLogs;

var connectivityResult;
bool nc = false;
int groupSize = 0;

void _enablePlatformOverrideForDesktop() {
  if (!kIsWeb && (Platform.isWindows || Platform.isLinux)) {
    debugDefaultTargetPlatformOverride = TargetPlatform.fuchsia;
  }
}

void main() {
  // This app is designed only to work vertically, so we limit
  // orientations to portrait up and down.
  _enablePlatformOverrideForDesktop();
  LogsRepository lr = LogsRepository();
  userLogs = lr.loadLogs();

  return runApp(FunkyMunchApp());

  /** SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    return runApp(FunkyMunchApp());
  });*/
}

class FunkyMunchApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    createDatabase();

    return MaterialApp(
      routes: {
        '/': (_) => InitPage(),
        'create': (_) => CreatePage(),
        'login': (_) => LoginPage(),
        'home': (_) => HomePage(),
        'pager': (_) => PagerPage(),
        'qrpager': (_) => PagerQRPage(),
        'register': (_) => RegisterPage(),
        'qrqueue': (_) => QueueQRPage(),
        'queue': (_) => QueuePage(),
        'logs': (_) => LogsPage(),
      },
    );
  }
}

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          'Funky Munch Home',
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        backgroundColor: Colors.black,
      ),
      body: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(height: 10, width: 1000),
          GestureDetector(
            onTap: () {
              Future.delayed(Duration.zero, () {
                Navigator.pushNamed(context, 'qrpager');
              });
            },
            child: PagerButton(),
          ),
          GestureDetector(
            onTap: () {
              Future.delayed(Duration.zero, () {
                Navigator.pushNamed(context, 'qrqueue');
              });
            },
            child: QueueButton(),
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(height: 20, width: 50),
              GestureDetector(
                onTap: () {
                  Future.delayed(Duration.zero, () {
                    Navigator.pushNamed(context, 'logs');
                  });
                },
                child: LogsButton(),
              ),
              GestureDetector(
                onTap: () {
                  showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: new Text('Logging out'),
                        content: new Text('Do you wish to log out?'),
                        actions: [
                          new FlatButton(
                              onPressed: () {
                                Future.delayed(Duration.zero, () {
                                  Navigator.of(context).pop();
                                });
                              },
                              child: Text('Cancel')),
                          new FlatButton(
                              onPressed: () {
                                Future.delayed(Duration.zero, () {
                                  Navigator.popUntil(
                                      context, ModalRoute.withName('/'));
                                });
                              },
                              child: Text('Accept'))
                        ],
                      );
                    },
                  );
                },
                child: LogoutButton(),
              ),
              SizedBox(height: 20, width: 50),
            ],
          ),

          //CupertinoButton(
          //  child: Text('Take me deeper!'),
          //  onPressed: () => Navigator.pushNamed(context, 'deeper'),
          // ),
        ],
      ),
    );
  }
}

class InitPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    if (!loggedIn) {
      return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text(
            'Welcome to Funky Munch',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.white,
            ),
          ),
          backgroundColor: Colors.black,
        ),
        body: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(height: 5, width: 1000),
              RaisedButton(
                child: Text('Create an account'),
                //onPressed: () => Navigator.pushNamed(context, 'create'),
                onPressed: () => Future.delayed(Duration.zero, () {
                  Navigator.pushNamed(context, 'create');
                }),
                color: Color.fromRGBO(0, 172, 255, 20),
              ),
              RaisedButton(
                child: Text('Log In'),
                onPressed: () => Future.delayed(Duration.zero, () {
                  Navigator.pushNamed(context, 'login');
                }),
                color: Color.fromRGBO(0, 172, 255, 20),
              ),
            ]),
      );
    } else {
      return HomePage();
    }
  }
}

class CreatePage extends StatefulWidget {
  @override
  CreatePageState createState() => CreatePageState();
}

class LoginPage extends StatefulWidget {
  @override
  LoginPageState createState() => LoginPageState();
}

class LoginPageState extends State<LoginPage> {
  final emailController = TextEditingController();
  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          'Log in',
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        backgroundColor: Colors.black,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(height: 30, width: 1000),
          TextField(
            controller: emailController,
            decoration: InputDecoration(
              hintText: 'Enter your registered email',
            ),
          ),
          RaisedButton(
            onPressed: () {
              var emailu = emailController.text;
              loadUser(emailu);
              User u = actual;
              if (u == null) {
                return AlertDialog(
                  title: new Text('Error'),
                  content: new Text('Your email is not registered in this phone'),
                  actions: [
                    new FlatButton(
                      onPressed:(){
                        Future.delayed(Duration.zero, (){
                          Navigator.of(context).pop();
                        });
                      },
                      child: Text('OK'),
                    ),
                  ],
                );
              } else {
                Future.delayed(Duration.zero, () {
                  Navigator.pushNamed(context, 'home');
                });
              }
            },
            child: Text('Enter'),
            color: Color.fromRGBO(0, 172, 255, 20),
          ),
        ],
      ),
    );
  }
}

class CreatePageState extends State<CreatePage> {
  final format = DateFormat("yyyy-MM-dd");
  DateTime selectedDate = DateTime.now();

  TextEditingController _date = new TextEditingController();
  final nombreController = TextEditingController();
  final sexoController = TextEditingController();
  final emailController = TextEditingController();
  var res = false;

  void disp() {
    nombreController.dispose();
    sexoController.dispose();
    emailController.dispose();
    _date.dispose();
  }

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(1901, 1),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }

  newUser() {
    int idu = (new DateTime.now()).millisecondsSinceEpoch;
    var nombreu = nombreController.text;
    var sexou = sexoController.text;
    var sdu = selectedDate;
    var emailu = emailController.text;

    User nu = new User(
      id: idu,
      nombre: nombreu,
      sexo: sexou,
      nacimiento: sdu,
      email: emailu,
    );
    storeUser(idu, nombreu, sexou, sdu, emailu);
    createUser(nu);
    actual = nu;
    disp();
  }

  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          'Create your account',
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        backgroundColor: Colors.black,
        leading: BackButton(
          color: Colors.white,
          onPressed: () => Future.delayed(Duration.zero, () {
            Navigator.popUntil(context, ModalRoute.withName('/'));
          }),
        ),
      ),
      body: Column(
        //mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(height: 20, width: 1000),
          TextField(
            controller: nombreController,
            decoration: InputDecoration(
              hintText: 'Enter your name',
            ),
          ),
          TextField(
            controller: sexoController,
            decoration: InputDecoration(
              hintText: 'Enter your sex',
            ),
          ),
          Text('Select your birthday (${format.pattern})'),
          RaisedButton(
            onPressed: () => _selectDate(context),
            child: Text('Select date'),
            color: Color.fromRGBO(0, 172, 255, 20),
          ),
          Text("Selected date: " + "${selectedDate.toLocal()}".split(' ')[0]),
          TextField(
            controller: emailController,
            decoration: InputDecoration(hintText: 'Enter your email'),
          ),
          SizedBox(height: 10),
          RaisedButton(
            onPressed: () {
              checkConnection();
              if(nc == true){
                return AlertDialog(
                  title: new Text('Error'),
                  content: new Text('There is no internet connection. Check your wifi and your mobile data.'),
                  actions: [
                    new FlatButton(
                      onPressed:(){
                        Future.delayed(Duration.zero, (){
                          Navigator.of(context).pop();
                        });
                      },
                      child: Text('OK'),
                    ),
                  ],
                );
              }else{
              newUser();
              Future.delayed(Duration.zero, () {
                Navigator.pushNamed(context, 'home');
              });
              dispose();
              loggedIn = true;
            }},
            child: Text('Submit your data'),
            color: Color.fromRGBO(0, 172, 255, 20),
          )
        ],
      ),
    );
  }
}

class PagerPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    changeStatePagerPage();
    changeStateTablePage();
    int idl = (new DateTime.now()).millisecondsSinceEpoch;
    int uid = actual.id;
    String rest = "RestauranteA";
    String t = "Pager";
    Log l = new Log(idl, uid, rest, hpager, t);
    userLogs.add(l);

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          'Pager',
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        backgroundColor: Colors.black,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          SizedBox(height: 20),
          Text(
            'Your order will be ready in approximately: ',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 16,
            ),
          ),
          SizedBox(height: 10),
          Text(
            '10 minutes',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 16,
            ),
          ),
          SizedBox(
            height: 30,
            width: 1000,
          ),
          Text(
            'Your phone will vibrate when the order is ready!',
            textAlign: TextAlign.center,
          ),
          RaisedButton(
            onPressed: () => Future.delayed(Duration.zero, () {
              Navigator.popUntil(context, ModalRoute.withName('home'));
            }),
            child: Text('Simulate end of service'),
            color: Color.fromRGBO(0, 172, 255, 20),
          ),
        ],
      ),
    );
  }
}

class PagerQRPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    checkConnection();
    if(nc == false){
    int idp = (new DateTime.now()).millisecondsSinceEpoch;
    int tp = 10;
    DateTime hora = formato.parse(formato.format(DateTime.now()));
    hpager = formato.parse(formato.format(DateTime.now()));
    String rest = "RestauranteA";
    Pager p = new Pager(
      id: idp,
      turno: tp,
      horaPedido: hora,
      restaurante: rest,
    );
    createPager(p);
    //Map<String, dynamic> info = p.toJson();
    //String qrd = json.encode(info);
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Your pager QR code',
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        backgroundColor: Colors.black,
      ),
      body: Column(
        children: [
          SizedBox(height: 10, width: 1000),
          new QrImage(
            data: "Inserte datos aqui",
            size: 200,
          ),
          RaisedButton(
            onPressed: () => Future.delayed(Duration.zero, () {
              Navigator.pushNamed(context, 'pager');
            }),
            child: Text('Simulate QR Scan'),
            color: Color.fromRGBO(0, 172, 255, 20),
          ),
        ],
      ),
    );
    }else{
      return NoConnectionPage();
    }
  }
}

class RegisterPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    checkConnection();
    if(nc == false){
    final sizeController = TextEditingController();
    return Scaffold(
      //backgroundColor: Color.fromRGBO(255, 139, 1, 20),
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          'Enter the size of your group',
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        backgroundColor: Colors.black,
        leading: BackButton(
          color: Colors.white,
          onPressed: () => Future.delayed(Duration.zero, () {
            Navigator.popUntil(context, ModalRoute.withName('home'));
          }),
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(height: 100),
          Text(
            'What is the size of your group?',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 14,
            ),
          ),
          SizedBox(height: 20),
          TextField(
            controller: sizeController,
            decoration: InputDecoration(hintText: 'size'),
          ),
          RaisedButton(
            child: Text('Enter'),
            color: Color.fromRGBO(0, 172, 255, 20),
            onPressed: () {
              groupSize = int.parse(sizeController.text);
              Future.delayed(Duration.zero, () {
                Navigator.pushNamed(context, 'queue');
              });
            },
          ),
        ],
      ),
    );
  }else{
      return NoConnectionPage();
    }
  }
}

class QueueQRPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    checkConnection();
    if(nc == false){
    Random random = new Random();
    int idt = (new DateTime.now()).millisecondsSinceEpoch;
    htable = formato.parse(formato.format(DateTime.now()));
    int nmt = random.nextInt(20) + 1;
    int npt = groupSize;
    DateTime hora = formato.parse(formato.format(DateTime.now()));
    String rest = "RestauranteA";

    Tables t = new Tables(
      id: idt,
      numeroMesa: nmt,
      numeroPersonas: npt,
      horaReserva: hora,
      restaurante: rest,
    );

    createTable(t);
    //Map<String, dynamic> info = t.toJson();
    //String qrd = json.encode(info);
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Your queue QR code',
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        backgroundColor: Colors.black,
      ),
      body: Column(
        children: [
          SizedBox(height: 10, width: 1000),
          new QrImage(
            data: "Inserte datos aqui",
            size: 200,
          ),
          RaisedButton(
            onPressed: () => Future.delayed(Duration.zero, () {
              Navigator.pushNamed(context, 'queue');
            }),
            child: Text('Simulate QR Scan'),
            color: Color.fromRGBO(0, 172, 255, 20),
          ),
        ],
      ),
    );
  }else{
    return NoConnectionPage();
  }
  }
}

class QueuePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    changeStateTablePage();
    int idl = (new DateTime.now()).millisecondsSinceEpoch;
    int uid = actual.id;
    String rest = "RestauranteA";
    String t = "Table";
    Log l = new Log(idl, uid, rest, htable, t);
    userLogs.add(l);

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          'Queue',
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        backgroundColor: Colors.black,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          SizedBox(height: 20),
          Text(
            'Congrats! You are in the queue now',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 16,
            ),
          ),
          SizedBox(height: 10),
          Text(
            'There are 5 groups ahead of you',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 16,
            ),
          ),
          SizedBox(height: 30),
          Text(
            'Your phone will vibrate when a table is ready for you and your group!',
            textAlign: TextAlign.center,
          ),
          RaisedButton(
            onPressed: () => Future.delayed(Duration.zero, () {
              Navigator.popUntil(context, ModalRoute.withName('home'));
            }),
            child: Text('Simulate end of service'),
            color: Color.fromRGBO(0, 172, 255, 20),
          ),
        ],
      ),
    );
  }
}

class LogsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    checkConnection();
    if (nc == false){
      return Scaffold(
          appBar: AppBar(
            title: Text(
              'Your logs',
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.white,
              ),
            ),
            backgroundColor: Colors.black,
            leading: BackButton(
              color: Colors.white,
              onPressed: () => Future.delayed(Duration.zero, () {
                Navigator.popUntil(context, ModalRoute.withName('home'));
              }),
            ),
          ),
          body: Scaffold(
            body: ListView.builder(
              // Let the ListView know how many items it needs to build.
              itemCount: userLogs.length,
              // Provide a builder function. This is where the magic happens.
              // Convert each item into a widget based on the type of item it is.
              itemBuilder: (context, index) {
                final item = userLogs[index];

                return ListTile(
                  leading: new Icon(MdiIcons.book),
                  title: item.buildName(context),
                  subtitle: item.buildDate(context),
                );
              },
            ),
          ));
    }else{
      return NoConnectionPage();
    }
  }
}

class NoConnectionPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'No connection',
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        backgroundColor: Colors.black,
        leading: BackButton(
          color: Colors.white,
          onPressed: () => Future.delayed(Duration.zero, () {
            Navigator.popUntil(context, ModalRoute.withName('home'));
          }),
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          SizedBox(height: 50, width: 1000),
          Icon(
            MdiIcons.signalOff,
            color: Colors.red,
            size: 50.0,
          ),
          Text(
              'You have no internet connection. Check if you are on plane mode or perhaps you are out of the wifi range or your mobile data is off'),
        ],
      ),
    );
  }
}

class PagerButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 200,
      height: 100,
      child: Text(
        'Use my phone as pager',
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: 16,
        ),
      ),
      decoration: BoxDecoration(
        color: Color.fromRGBO(0, 172, 255, 20),
        border: Border.all(),
      ),
    );
  }
}

class QueueButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 200,
      height: 100,
      child: Text(
        'Virtual Table Queue',
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: 16,
        ),
      ),
      decoration: BoxDecoration(
        color: Color.fromRGBO(0, 172, 255, 20),
        border: Border.all(),
      ),
    );
  }
}

class LogsButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 100,
      height: 100,
      child: Text(
        'My Logs',
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: 16,
        ),
      ),
      decoration: BoxDecoration(
        color: Color.fromRGBO(0, 172, 255, 20),
        border: Border.all(),
      ),
    );
  }
}

class LogoutButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 100,
      height: 100,
      child: Text(
        'Log out',
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: 16,
        ),
      ),
      decoration: BoxDecoration(
        color: Color.fromRGBO(0, 172, 255, 20),
        border: Border.all(),
      ),
    );
  }
}

checkConnection() async {
  connectivityResult = await (Connectivity().checkConnectivity());
  if (connectivityResult == ConnectivityResult.mobile) {
    nc = false;
  } else if (connectivityResult == ConnectivityResult.wifi) {
    nc = false;
  } else {
    nc = true;
  }
}

changeStatePagerPage() {
  pagerActive = true;
}

changeStateTablePage() {
  tableActive = true;
}

createDatabase() async {
  String databasesPath = await getDatabasesPath();
  String dbPath = join(databasesPath, 'funkymunch.db');

  var database = await openDatabase(dbPath, version: 1, onCreate: populateDb);
  return database;
}

void populateDb(Database database, int version) async {
  await database.execute("CREATE TABLE User ("
      "id INTEGER PRIMARY KEY,"
      "Nombre TEXT,"
      "Sexo TEXT,"
      "Nacimiento DATE,"
      "Email TEXT,"
      ")");

  await database.execute("CREATE TABLE Pager ("
      "id INTEGER PRIMARY KEY,"
      "Turno INTEGER,"
      "HoraPedido DATE,"
      "Restaurante TEXT,"
      ")");

  await database.execute("CREATE TABLE Table ("
      "id INTEGER PRIMARY KEY,"
      "NumeroMesa INTEGER,"
      "NumeroPersonas INTEGER,"
      "HoraReserva DATE,"
      "Restaurante TEXT,"
      ")");

  await database.close();
}

storeUser(
    int idu, String nombreu, String sexu, DateTime nacu, String mailu) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.setInt('id', idu);
  await prefs.setString('nombre', nombreu);
  await prefs.setString('sexo', sexu);
  await prefs.setString('nacimiento', nacu.toString());
  await prefs.setString('email', mailu);
}

loadUser(String mail) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String reg = prefs.getString('email');
  if (reg != mail) {
    return null;
  }
  int idu = prefs.getInt('id');
  String nombreu = prefs.getString('nombre');
  String sexu = prefs.getString('sexo');
  DateTime nacu = formato.parse(prefs.getString('nacimiento'));
  String mailu = prefs.getString('email');

  var u = new User(
    id: idu,
    nombre: nombreu,
    sexo: sexu,
    nacimiento: nacu,
    email: mailu,
  );

  actual = u;
}

//CRUD User

Future<int> createUser(User user) async {
  var result = await database.insert("User", user.toJson());
  return result;
}

Future<List> getUsers() async {
  var result = await database
      .query("User", columns: ["id", "Nombre", "Sexo", "Nacimiento", "Email"]);

  return result.toList();
}

Future<User> getUser(int id) async {
  List<Map> results = await database.query("User",
      columns: ["id", "Nombre", "Sexo", "Nacimiento", "Email"],
      where: 'id = ?',
      whereArgs: [id]);

  if (results.length > 0) {
    return new User.fromJson(results.first);
  }

  return null;
}

Future<User> getUserByEmail(String mail) async {
  List<Map> results = await database.query("User",
      columns: ["id", "Nombre", "Sexo", "Nacimiento", "Email"],
      where: 'email = ?',
      whereArgs: [mail]);

  if (results.length > 0) {
    return new User.fromJson(results.first);
  }

  return null;
}

Future<int> updateUser(User user) async {
  return await database
      .update("User", user.toJson(), where: "id = ?", whereArgs: [user.id]);
}

Future<int> deleteUser(int id) async {
  return await database.delete("User", where: 'id = ?', whereArgs: [id]);
}

//CRUD Pager

Future<int> createPager(Pager pager) async {
  var result = await database.insert("Pager", pager.toJson());
  return result;
}

Future<List> getPagers() async {
  var result = await database.query("Pager",
      columns: ["id", "Turno", "HoraPedido", "Precio", "Restaurante"]);

  return result.toList();
}

Future<User> getPager(int id) async {
  List<Map> results = await database.query("Pager",
      columns: ["id", "Turno", "HoraPedido", "Precio", "Restaurante"],
      where: 'id = ?',
      whereArgs: [id]);

  if (results.length > 0) {
    return new User.fromJson(results.first);
  }

  return null;
}

Future<int> updatePager(Pager pager) async {
  return await database
      .update("Pager", pager.toJson(), where: "id = ?", whereArgs: [pager.id]);
}

Future<int> deletePager(int id) async {
  return await database.delete("Pager", where: 'id = ?', whereArgs: [id]);
}

//CRUD Table

Future<int> createTable(Tables table) async {
  var result = await database.insert("Table", table.toJson());
  return result;
}

Future<List> getTables() async {
  var result = await database.query("Table", columns: [
    "id",
    "NumeroMesa",
    "NumeroPersonas",
    "HoraReserva",
    "Restaurante"
  ]);

  return result.toList();
}

Future<User> getTable(int id) async {
  List<Map> results = await database.query("Table",
      columns: [
        "id",
        "NumeroMesa",
        "NumeroPersonas",
        "HoraReserva",
        "Restaurante"
      ],
      where: 'id = ?',
      whereArgs: [id]);

  if (results.length > 0) {
    return new User.fromJson(results.first);
  }

  return null;
}

Future<int> updateTable(Tables table) async {
  return await database
      .update("Table", table.toJson(), where: "id = ?", whereArgs: [table.id]);
}

Future<int> deleteTable(int id) async {
  return await database.delete("Table", where: 'id = ?', whereArgs: [id]);
}
